const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

// [ACTIVITY - s47]
const fullName = () => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = firstName + ' ' + lastName;

}

txtFirstName.addEventListener('keyup', fullName);
txtLastName.addEventListener('keyup', fullName);
